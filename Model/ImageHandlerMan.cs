﻿using System.Collections.Generic;

namespace Hangman.Model
{
    class ImageHandlerMan
    {
        List<string> m_images;
        int m_position;

        public ImageHandlerMan()
        {
            m_images = new List<string>();

            m_images.Add("/manWrongs/ZeroWrong.png");
            m_images.Add("/manWrongs/OneWrong.png");
            m_images.Add("/manWrongs/TwoWrong.png");
            m_images.Add("/manWrongs/ThreeWrong.png");
            m_images.Add("/manWrongs/FourWrong.png");
            m_images.Add("/manWrongs/FiveWrong.png");
            m_images.Add("/manWrongs/SixWrong.png");
        }

        public string ActualImage
        {
            get
            {
                return m_images[Position];
            }
        }

        public string NextImage
        {
            get
            {
                ++Position;

                return ActualImage;
            }
        }

        public string ResetImage
        {
            get
            {
                Position = 0;

                return ActualImage;
            }
        }

        private int Position
        {
            get
            {
                return m_position;
            }
            set
            {
                if (value >= m_images.Count - 1)
                    m_position = m_images.Count - 1;
                else
                    m_position = value;
            }
        }


    }
}
