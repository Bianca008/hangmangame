﻿using System.Collections.Generic;

namespace Hangman.Model
{
    class ImageHandler
    {
        List<string> m_images;
        int m_position;

        public ImageHandler()
        {
            m_images = new List<string>();

            m_images.Add("/images/avatar1.jpg");
            m_images.Add("/images/avatar2.jpg");
            m_images.Add("/images/avatar3.jpg");
            m_images.Add("/images/avatar4.jpg");
            m_images.Add("/images/avatar5.jpg");
            m_images.Add("/images/avatar6.jpg");
            m_images.Add("/images/avatar7.jpg");
            m_images.Add("/images/avatar8.jpg");
            m_images.Add("/images/avatar9.jpg");
            m_images.Add("/images/avatar10.jpg");
            m_images.Add("/images/avatar11.jpg");
            m_images.Add("/images/avatar12.jpg");

            m_position = m_images.Count / 2;
        }

        public string ActualImage
        {
            get
            {
                return m_images[Position];
            }
            set
            {
                m_images[Position] = value;
            }
        }

        public string NextImage
        {
            get
            {
                ++Position;
                return m_images[Position];
            }
        }

        public string PreviousImage
        {
            get
            {
                --Position;
                return m_images[Position];
            }
        }

        private int Position
        {
            get
            {
                return m_position;
            }
            set
            {
                if (value < 0)
                    m_position = 0;
                else
                if (value >= m_images.Count - 1)
                    m_position = m_images.Count - 1;
                else
                    m_position = value;
            }
        }
    }
}
