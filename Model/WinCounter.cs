﻿namespace Hangman.Model
{
    class WinCounter
    {
        public int TotalWins
        {
            get;
            set;
        }

        public void NewWin()
        {
            ++TotalWins;
        }

        public void ResetWins()
        {
            TotalWins = 0;
        }

    }
}
