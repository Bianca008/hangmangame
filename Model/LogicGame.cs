﻿using HangmanGame.Model;
using System.Collections.Generic;

namespace Hangman.Model
{
    class LogicGame
    {
        List<SaveGame> m_saveGameStatus;
        List<Statistics> m_stats;
        WinCounter m_counter;
        CategoriesReader m_categories;
        string m_wordToGuess;
        string m_wrongCharacters;
        string m_actualCategory;

        public enum GameStatus
        {
            STILL_GUESSING,
            NOT_GUESSED,
            WIN_LEVEL,
            WIN,
        }

        public LogicGame(string playerName)
        {
            PlayerName = playerName;
            m_saveGameStatus = new List<SaveGame>();
            m_categories = new CategoriesReader();
            m_counter = new WinCounter();
            ActualCategory = "All";
            m_wordToGuess = m_categories.WordFromAllCategories;
            UserWord = new string('*', m_wordToGuess.Length);
        }

        private List<int> GetAllIndexesOfCharacter(char character)
        {
            List<int> indexes = new List<int>();

            for (int index = 0; index < m_wordToGuess.Length; ++index)
                if (m_wordToGuess[index] == character)
                    indexes.Add(index);

            return indexes;
        }

        public void ModifyWord(char character)
        {
            List<int> indexes = GetAllIndexesOfCharacter(character);

            if (indexes.Count == 0 && m_wrongCharacters.Contains(character.ToString()) == false)
            {
                m_wrongCharacters += character;
                ++NumberOfWrongs;
                return;
            }

            for (int index = 0; index < indexes.Count; ++index)
                if (indexes[index] < UserWord.Length - 1)
                    UserWord = UserWord.Substring(0, indexes[index]) + m_wordToGuess[indexes[index]] + UserWord.Substring(indexes[index] + 1);
                else
                    UserWord = UserWord.Substring(0, indexes[index]) + m_wordToGuess[indexes[index]];
        }

        private void ResetLevel()
        {
            NumberOfWrongs = 0;
            WrongCharacters = "";

            switch (ActualCategory)
            {
                case "Animals":
                    {
                        m_wordToGuess = m_categories.AnimalsCategory;
                        break;
                    }
                case "Cars":
                    {
                        m_wordToGuess = m_categories.CarsCategory;
                        break;
                    }
                case "Flowers":
                    {
                        m_wordToGuess = m_categories.FlowersCategory;
                        break;
                    }
                case "Insects":
                    {
                        m_wordToGuess = m_categories.InsectsCategory;
                        break;
                    }
                case "Movies":
                    {
                        m_wordToGuess = m_categories.MoviesCategory;
                        break;
                    }
                default:
                    {
                        m_wordToGuess = m_categories.WordFromAllCategories;
                        break;
                    }
            }

            UserWord = new string('*', m_wordToGuess.Length);
        }

        public void ResetGame()
        {
            m_counter.ResetWins();
            ResetLevel();
        }

        public GameStatus IsEnd()
        {
            if (WrongCharacters.Length == "Wrong characters: ".Length + 6)
            {
                GameStats(false);
                ResetGame();
                return GameStatus.NOT_GUESSED;
            }
            else
            if (m_wordToGuess == UserWord)
            {
                m_counter.NewWin();
                GameStats(true);
                if (m_counter.TotalWins == 5)
                {
                    ResetGame();
                    return GameStatus.WIN;
                }
                ResetLevel();
                return GameStatus.WIN_LEVEL;
            }

            return GameStatus.STILL_GUESSING;
        }

        private void WinStatementStats(Statistics stat)
        {
            switch (ActualCategory)
            {
                case "Animals":
                    {
                        stat.AnimalsCategory += 1;
                        break;
                    }
                case "Cars":
                    {
                        stat.CarsCategory += 1;
                        break;
                    }
                case "Flowers":
                    {
                        stat.FlowersCategory += 1;
                        break;
                    }
                case "Insects":
                    {
                        stat.InsectsCategory += 1;
                        break;
                    }
                case "Movies":
                    {
                        stat.MoviesCategory += 1;
                        break;
                    }
                default:
                    {
                        stat.AllCategories += 1;
                        break;
                    }
            }
        }

        public string GetStats()
        {
            string str = "";
            DeserializeStatistics("statistics.bin");

            for (int index = 0; index < m_stats.Count; ++index)
                if (m_stats[index].Name == PlayerName)
                {
                    str += m_stats[index].Name + " played " + m_stats[index].GamesPlayed +
                        " where wins " + m_stats[index].AllCategories + " are from all categories, " +
                        m_stats[index].AnimalsCategory + " are from animals category " +
                        m_stats[index].CarsCategory + " are from cars category " +
                        m_stats[index].FlowersCategory + " are from flowers category " +
                        m_stats[index].InsectsCategory + " are from insects category " +
                        m_stats[index].MoviesCategory + " are from movies category.";
                }

            return str;
        }

        public void GameStats(bool win)
        {
            DeserializeStatistics("statistics.bin");

            for (int index = 0; index < m_stats.Count; ++index)
                if (m_stats[index].Name == PlayerName)
                {
                    if (m_counter.TotalWins == 5 || win == false)
                        m_stats[index].GamesPlayed += 1;
                    if (m_counter.TotalWins == 5)
                        WinStatementStats(m_stats[index]);

                    SerializeStatistics("statistics.bin");
                    break;
                }
        }

        public void SaveGameHistoric(int numberOfSecond)
        {
            SaveGame stats = new SaveGame();

            stats.Name = PlayerName;
            stats.Category = ActualCategory;
            stats.NumberOfMistakes = NumberOfWrongs;
            stats.Wrongs = WrongCharacters;
            stats.NumberOfGames += 1;
            stats.NumberOfWinsPerLevel = m_counter.TotalWins;
            stats.ActualWord = m_wordToGuess;
            stats.UserWord = UserWord;

            if (numberOfSecond != -1)
                stats.TimeRemained = numberOfSecond;
            else
                stats.TimeRemained = 0;

            Deserialize("historic.bin");

            m_saveGameStatus.Add(stats);

            Serialize("historic.bin");
        }

        public string TimeRemained
        {
            get;
            set;
        }

        public void GetGame()
        {
            Deserialize("historic.bin");

            for (int index = 0; index < m_saveGameStatus.Count; ++index)
                if (m_saveGameStatus[index].Name == PlayerName)
                {
                    ActualCategory = m_saveGameStatus[index].Category;
                    WrongCharacters = m_saveGameStatus[index].Wrongs.Substring(m_saveGameStatus[index].Wrongs.IndexOf(':') + 2);
                    NumberOfWrongs = m_saveGameStatus[index].NumberOfMistakes;
                    m_counter.TotalWins = m_saveGameStatus[index].NumberOfWinsPerLevel;
                    m_wordToGuess = m_saveGameStatus[index].ActualWord;
                    UserWord = m_saveGameStatus[index].UserWord;
                    TimeRemained = "Time:" + m_saveGameStatus[index].TimeRemained.ToString();
                    Serialize("historic.bin");
                }
        }

        private void Serialize(string file)
        {
            ObjectToSerialize<SaveGame> objectToSerialize = new ObjectToSerialize<SaveGame>();
            objectToSerialize.ObjectsList = m_saveGameStatus;
            Serializer<SaveGame> serializer = new Serializer<SaveGame>();
            serializer.SerializeObject(file, objectToSerialize);
        }

        private void Deserialize(string file)
        {
            Serializer<SaveGame> serializer = new Serializer<SaveGame>();
            ObjectToSerialize<SaveGame> objectToSerialize = serializer.DeserializeObject(file);
            m_saveGameStatus = objectToSerialize.ObjectsList;
        }

        private void SerializeStatistics(string file)
        {
            ObjectToSerialize<Statistics> objectToSerialize = new ObjectToSerialize<Statistics>();
            objectToSerialize.ObjectsList = m_stats;
            Serializer<Statistics> serializer = new Serializer<Statistics>();
            serializer.SerializeObject(file, objectToSerialize);
        }

        private void DeserializeStatistics(string file)
        {
            Serializer<Statistics> serializer = new Serializer<Statistics>();
            ObjectToSerialize<Statistics> objectToSerialize = serializer.DeserializeObject(file);
            m_stats = objectToSerialize.ObjectsList;
        }

        public string WrongCharacters
        {
            get
            {
                return "Wrong characters: " + m_wrongCharacters;
            }
            set
            {
                m_wrongCharacters = value;
            }
        }

        public int NumberOfWrongs
        {
            get;
            set;
        }

        public string UserWord
        {
            get;
            set;
        }

        public string ActualCategory
        {
            get
            {
                return m_actualCategory;
            }
            set
            {
                m_actualCategory = value;
                ResetGame();
            }
        }

        public string ChangeToAllCategories
        {
            get
            {
                m_wordToGuess = m_categories.WordFromAllCategories;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "All";

                return UserWord;
            }
        }

        public string ChangeToAnimalsCategory
        {
            get
            {
                m_wordToGuess = m_categories.AnimalsCategory;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "Animals";

                return UserWord;
            }
        }

        public string ChangeToCarsCategory
        {
            get
            {
                m_wordToGuess = m_categories.CarsCategory;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "Cars";

                return UserWord;
            }
        }

        public string ChangeToFlowersCategory
        {
            get
            {
                m_wordToGuess = m_categories.FlowersCategory;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "Flowers";

                return UserWord;
            }
        }

        public string ChangeToInsectsCategory
        {
            get
            {
                m_wordToGuess = m_categories.InsectsCategory;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "Insects";

                return UserWord;
            }
        }

        public string ChangeToMoviesCategory
        {
            get
            {
                m_wordToGuess = m_categories.MoviesCategory;
                UserWord = new string('*', m_wordToGuess.Length);
                ActualCategory = "Movies";

                return UserWord;
            }
        }

        private string PlayerName
        {
            get;
            set;
        }

        public int NumberOfLevel
        {
            get
            {
                return m_counter.TotalWins;
            }
        }
    }
}
