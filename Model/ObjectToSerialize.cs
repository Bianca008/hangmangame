﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HangmanGame.Model
{
    [Serializable]
    class ObjectToSerialize<T>: ISerializable
    {
        public List<T> ObjectsList
        { get; set; }

        public ObjectToSerialize() { }

        public ObjectToSerialize(SerializationInfo info, StreamingContext ctxt)
        {
            ObjectsList = (List<T>)info.GetValue("players", typeof(List<T>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("players", ObjectsList);
        }
    }
}
