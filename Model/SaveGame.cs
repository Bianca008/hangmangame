﻿using System;
using System.Runtime.Serialization;

namespace HangmanGame.Model
{
    [Serializable]
    class SaveGame : ISerializable
    {
        public SaveGame()
        {

        }

        public SaveGame(SerializationInfo info, StreamingContext ctxt)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            NumberOfGames = (int)info.GetValue("NumberOfWins", typeof(int));
            NumberOfWinsPerLevel = (int)info.GetValue("NumberOfWinsPerLevel", typeof(int));
            Category = (string)info.GetValue("Category", typeof(string));
            ActualWord = (string)info.GetValue("ActualWord", typeof(string));
            UserWord = (string)info.GetValue("UserWord", typeof(string));
            NumberOfMistakes = (int)info.GetValue("NumberOfMistakes", typeof(int));
            TimeRemained = (int)info.GetValue("TimeRemained", typeof(int));
            Wrongs = (string)info.GetValue("Wrongs", typeof(string));
        }

        public string Name
        {
            get;
            set;
        }

        public int NumberOfGames
        {
            get;
            set;
        }

        public string Wrongs
        {
            get;
            set;
        }

        public int NumberOfWinsPerLevel
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }

        public string ActualWord
        {
            get;
            set;
        }

        public string UserWord
        {
            get;
            set;
        }

        public int NumberOfMistakes
        {
            get;
            set;
        }

        public int TimeRemained
        {
            get;
            set;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("NumberOfWins", NumberOfGames);
            info.AddValue("NumberOfWinsPerLevel", NumberOfWinsPerLevel);
            info.AddValue("Category", Category);
            info.AddValue("ActualWord", ActualWord);
            info.AddValue("UserWord", UserWord);
            info.AddValue("NumberOfMistakes", NumberOfMistakes);
            info.AddValue("Wrongs", Wrongs);
            info.AddValue("TimeRemained", TimeRemained);
        }
    }
}
