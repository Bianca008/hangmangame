﻿using System;
using System.Runtime.Serialization;

namespace HangmanGame.Model
{
    [Serializable]

    class Statistics : ISerializable
    {
        public string Name
        {
            get;
            set;
        }

        public int GamesPlayed
        {
            get;
            set;
        }

        public int AllCategories
        {
            get;
            set;
        }

        public int AnimalsCategory
        {
            get;
            set;
        }

        public int CarsCategory
        {
            get;
            set;
        }

        public int FlowersCategory
        {
            get;
            set;
        }

        public int InsectsCategory
        {
            get;
            set;
        }

        public int MoviesCategory
        {
            get;
            set;
        }

        public Statistics()
        {

        }

        public Statistics(SerializationInfo info, StreamingContext ctxt)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            GamesPlayed = (int)info.GetValue("GamesPlayed", typeof(int));
            AllCategories = (int)info.GetValue("AllCategories", typeof(int));
            AnimalsCategory = (int)info.GetValue("AnimalsCategory", typeof(int));
            CarsCategory = (int)info.GetValue("CarsCategory", typeof(int));
            FlowersCategory = (int)info.GetValue("FlowersCategory", typeof(int));
            InsectsCategory = (int)info.GetValue("InsectsCategory", typeof(int));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("GamesPlayed", GamesPlayed);
            info.AddValue("AllCategories", AllCategories);
            info.AddValue("AnimalsCategory", AnimalsCategory);
            info.AddValue("CarsCategory", CarsCategory);
            info.AddValue("FlowersCategory", FlowersCategory);
            info.AddValue("InsectsCategory", InsectsCategory);
            info.AddValue("MoviesCategory", MoviesCategory);
        }
    }
}
