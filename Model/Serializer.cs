﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace HangmanGame.Model
{
    class Serializer<T>
    {
        public void SerializeObject(string filename, ObjectToSerialize<T> objectToSerialize)
        {
            using (Stream stream = File.Open(filename, FileMode.Create))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, objectToSerialize);
            }
        }

        public ObjectToSerialize<T> DeserializeObject(string filename)
        {
            ObjectToSerialize<T> objectToSerialize;
            using (Stream stream = File.Open(filename, FileMode.Open))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToSerialize = (ObjectToSerialize<T>)bFormatter.Deserialize(stream);
            }
            return objectToSerialize;
        }

    }
}
