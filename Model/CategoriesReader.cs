﻿using System;
using System.Collections.Generic;

namespace Hangman.Model
{
    class CategoriesReader
    {
        List<string> m_animals;
        List<string> m_cars;
        List<string> m_flowers;
        List<string> m_insects;
        List<string> m_movies;

        private List<string> Read(string file)
        {
            List<string> values = new List<string>();

            System.IO.StreamReader inputFile = new System.IO.StreamReader(file);

            string word;

            while ((word = inputFile.ReadLine()) != null)
                values.Add(word);

            return values;
        }

        public CategoriesReader()
        {
            m_animals = Read("resources/Animals.txt");
            m_cars = Read("resources/Cars.txt");
            m_insects = Read("resources/Insects.txt");
            m_flowers = Read("resources/Flowers.txt");
            m_movies = Read("resources/Movies.txt");
        }

        public string WordFromAllCategories
        {
            get
            {
                List<string> allCategories = new List<string>(m_animals.Count +
                    m_cars.Count +
                    m_insects.Count +
                    m_flowers.Count +
                    m_movies.Count);

                allCategories.AddRange(m_animals);
                allCategories.AddRange(m_cars);
                allCategories.AddRange(m_insects);
                allCategories.AddRange(m_flowers);
                allCategories.AddRange(m_movies);

                Random random = new Random();
                int randomPosition = random.Next(0, allCategories.Count);

                return allCategories[randomPosition];
            }
        }

        public string AnimalsCategory
        {
            get
            {
                Random random = new Random();
                int randomPosition = random.Next(0, m_animals.Count);

                return m_animals[randomPosition];
            }
        }

        public string CarsCategory
        {
            get
            {
                Random random = new Random();
                int randomPosition = random.Next(0, m_cars.Count);

                return m_cars[randomPosition];
            }
        }

        public string FlowersCategory
        {
            get
            {
                Random random = new Random();
                int randomPosition = random.Next(0, m_flowers.Count);

                return m_flowers[randomPosition];
            }
        }

        public string InsectsCategory
        {
            get
            {
                Random random = new Random();
                int randomPosition = random.Next(0, m_insects.Count);

                return m_insects[randomPosition];
            }
        }

        public string MoviesCategory
        {
            get
            {
                Random random = new Random();
                int randomPosition = random.Next(0, m_movies.Count);

                return m_movies[randomPosition];
            }
        }
    }
}
