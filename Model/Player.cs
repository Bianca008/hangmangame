﻿using System;
using System.Runtime.Serialization;

namespace HangmanGame.Model
{
    [Serializable]

    class Player : ISerializable
    {
        public Player()
        {

        }

        public Player(SerializationInfo info, StreamingContext ctxt)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            Avatar = (string)info.GetValue("Avatar", typeof(string));
        }

        public string Name
        {
            get;
            set;
        }

        public string Avatar
        {
            get;
            set;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Avatar", Avatar);
        }
    }
}
