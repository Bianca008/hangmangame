﻿using HangmanGame.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Hangman
{
    public partial class MainWindow : Window
    {
        List<Player> m_players;

        public MainWindow()
        {
            InitializeComponent();
            this.Icon = BitmapFrame.Create(new Uri("hangman.ico", UriKind.RelativeOrAbsolute));
            //Initialization();
            m_players = new List<Player>();
            playersDataGrid.ItemsSource = m_players;
            Deserialize();
        }

        void Initialization()
        {
            Player firstPlayer = new Player();
            Player secondPlayer = new Player();
            Player thirdPlayer = new Player();
            Player fourthPlayer = new Player();

            firstPlayer.Name = "Razvan";
            firstPlayer.Avatar = "/images/avatar3.jpg";
            secondPlayer.Name = "Claudiu";
            secondPlayer.Avatar = "/images/avatar9.jpg";
            thirdPlayer.Name = "Nadia";
            thirdPlayer.Avatar = "/images/avatar12.jpg";
            fourthPlayer.Name = "Tessy";
            fourthPlayer.Avatar = "/images/avatar1.jpg";

            m_players = new List<Player>();

            m_players.Add(firstPlayer);
            m_players.Add(secondPlayer);
            m_players.Add(thirdPlayer);
            m_players.Add(fourthPlayer);

            SaveGame firstPlayerGame = new SaveGame();
            firstPlayerGame.ActualWord = "FIAT";
            firstPlayerGame.Category = "Cars";
            firstPlayerGame.Name = "Razvan";
            firstPlayerGame.NumberOfGames = 2;
            firstPlayerGame.NumberOfMistakes = 0;
            firstPlayerGame.NumberOfWinsPerLevel = 2;
            firstPlayerGame.TimeRemained = 20;
            firstPlayerGame.Wrongs = "Wrong characters: ";
            firstPlayerGame.UserWord = "**A*";

            m_savedGames = new List<SaveGame>();
            m_savedGames.Add(firstPlayerGame);

            Statistics statFirst = new Statistics();
            statFirst.GamesPlayed = 2;
            statFirst.AllCategories = 0;
            statFirst.AnimalsCategory = 2;
            statFirst.CarsCategory = 3;
            statFirst.FlowersCategory = 0;
            statFirst.MoviesCategory = 1;
            statFirst.Name = "Razvan";

            m_stats = new List<Statistics>();
            m_stats.Add(statFirst);

            SerializeStatistic();
            SerializeSavedGame();
        }

        public void Serialize()
        {
            DeserializeStatistics();
            ObjectToSerialize<Player> objectToSerialize = new ObjectToSerialize<Player>();
            objectToSerialize.ObjectsList = m_players;
            Serializer<Player> serializer = new Serializer<Player>();
            serializer.SerializeObject("names.bin", objectToSerialize);
            MessageBox.Show("Operation was a success!");
        }
        public void Deserialize()
        {
            Serializer<Player> serializer = new Serializer<Player>();
            ObjectToSerialize<Player> objectToSerialize = serializer.DeserializeObject("names.bin");
            m_players = objectToSerialize.ObjectsList;
            playersDataGrid.ItemsSource = m_players;
        }

        private Statistics IsInStats(List<string> players, int index)
        {
            for (int indexStat = 0; indexStat < m_stats.Count; ++indexStat)
                if (players[index] == m_stats[indexStat].Name)
                    return m_stats[indexStat];

            return null;
        }

        public void SerializeStatistic()
        {
            ObjectToSerialize<Statistics> objectToSerialize = new ObjectToSerialize<Statistics>();
            List<Statistics> stats = new List<Statistics>();
            List<string> availableNames = new List<string>();

            for (int index = 0; index < m_players.Count; ++index)
                availableNames.Add(m_players[index].Name);

            for (int index = 0; index < m_players.Count; ++index)
            {
                Statistics possibleStat = IsInStats(availableNames, index);
                if (possibleStat != null)
                    stats.Add(possibleStat);
                else
                {
                    Statistics newStat = new Statistics();
                    newStat.Name = m_players[index].Name;
                    stats.Add(newStat);
                }
            }

            objectToSerialize.ObjectsList = stats;
            Serializer<Statistics> serializer = new Serializer<Statistics>();
            serializer.SerializeObject("statistics.bin", objectToSerialize);
        }

        public void DeserializeStatistics()
        {
            Serializer<Statistics> serializer = new Serializer<Statistics>();
            ObjectToSerialize<Statistics> objectToSerialize = serializer.DeserializeObject("statistics.bin");
            m_stats = objectToSerialize.ObjectsList;
            SerializeStatistic();
        }

        List<SaveGame> m_savedGames;
        List<Statistics> m_stats;

        public void SerializeSavedGame()
        {
            ObjectToSerialize<SaveGame> objectToSerialize = new ObjectToSerialize<SaveGame>();
            List<SaveGame> games = new List<SaveGame>();
            List<string> availableNames = new List<string>();

            for (int index = 0; index < m_players.Count; ++index)
                availableNames.Add(m_players[index].Name);

            for (int index = 0; index < m_savedGames.Count; ++index)
            {
                if (availableNames.Contains(m_savedGames[index].Name) == true)
                    games.Add(m_savedGames[index]);
            }

            objectToSerialize.ObjectsList = games;
            Serializer<SaveGame> serializer = new Serializer<SaveGame>();
            serializer.SerializeObject("historic.bin", objectToSerialize);
        }

        public void DeserializeGame()
        {
            Serializer<SaveGame> serializer = new Serializer<SaveGame>();
            ObjectToSerialize<SaveGame> objectToSerialize = serializer.DeserializeObject("historic.bin");
            m_savedGames = objectToSerialize.ObjectsList;
            SerializeSavedGame();
        }

        private void AddPlayer(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            string name = button.Tag.ToString();
            string playerName = name.Substring(name.IndexOf(':') + 2);

            string sourceImage = avatarImage.Source.ToString();
            string avatar = sourceImage.Substring(sourceImage.IndexOf(',') + 3);

            if (playerName.IndexOf("TextBox") == -1)
            {
                Player newPlayer = new Player();
                newPlayer.Name = playerName;
                newPlayer.Avatar = avatar;

                m_players.Add(newPlayer);

                Serialize();
                Deserialize();
            }
        }

        private void DeletePlayer(object sender, RoutedEventArgs e)
        {
            Player toDelete = (Player)playersDataGrid.SelectedItem;

            if (toDelete != null)
            {
                int index = 0;
                while (index < m_players.Count)
                {
                    if (m_players[index].Name == toDelete.Name)
                        m_players.RemoveAt(index);
                    else
                        ++index;
                }

                Serialize();
                DeserializeStatistics();
                DeserializeGame();
                Deserialize();
            }
        }
    }
}
