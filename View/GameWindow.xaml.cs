﻿using Hangman.ViewModel;
using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Hangman.View
{
    public partial class GameWindow : Window
    {
        public GameWindow(string playerName, string playerAvatar)
        {
            InitializeComponent();
            avatarImage.Source = new BitmapImage(new Uri(playerAvatar, UriKind.RelativeOrAbsolute));
            userName.Text = playerName;
            this.Icon = BitmapFrame.Create(new Uri("hangman.ico", UriKind.RelativeOrAbsolute));
            DataContext = new CommandsHandlerGameWindow(playerName);
        }
    }
}
