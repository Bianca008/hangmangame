﻿using Hangman.Model;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Hangman.ViewModel
{
    class CommandsHandlerGameWindow : INotifyPropertyChanged
    {
        ICommand m_aboutCommand;
        ICommand m_changeCategory;
        ICommand m_characterPressed;
        ICommand m_exitGameWindow;
        ICommand m_newGame;
        ICommand m_openGame;
        ICommand m_saveGame;
        ICommand m_statistics;

        LogicGame m_gameLogic;
        ImageHandlerMan m_imageHandlerMan;
        int m_numberOfMistakes;
        string m_manImage;
        string m_currentWord;
        string m_timerText;

        DispatcherTimer m_timer;
        private DateTime m_deadline;
        private int m_numberOfSeconds = 31;

        public CommandsHandlerGameWindow(string playerName)
        {
            m_timer = new DispatcherTimer();
            m_timer.Tick += new EventHandler(dispatcherTimer_Tick);

            m_gameLogic = new LogicGame(playerName);
            m_imageHandlerMan = new ImageHandlerMan();

            m_currentWord = m_gameLogic.UserWord;
            m_manImage = m_imageHandlerMan.ActualImage;
        }

        public string TimerText
        {
            get
            {
                return m_timerText;
            }
            set
            {
                m_timerText = value;
                OnPropertyChanged("TimerText");
            }
        }

        public string NumberOfLevels
        {
            get
            {
                return "Level:" + m_gameLogic.NumberOfLevel;
            }
        }

        public string Category
        {
            get
            {
                return "Category:" + m_gameLogic.ActualCategory;
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            int secondsRemaining = (m_deadline - DateTime.Now).Seconds;

            if (secondsRemaining <= 0)
            {
                m_timer.Stop();
                m_timer.IsEnabled = false;
                TimerText = "Time: 0";
                m_numberOfSeconds = 31;
                m_gameLogic.ResetGame();
                m_gameLogic.GameStats(false);
                Reset();
                MessageBox.Show("Sorry! Please try again!");
            }
            else
            {
                TimerText = "Time:" + secondsRemaining.ToString();
            }
            CheckGameStatus();
        }

        public string WrongCharacterText
        {
            get
            {
                return m_gameLogic.WrongCharacters;
            }
        }

        public string ManImage
        {
            get
            {
                return m_manImage;
            }

            private set
            {
                m_manImage = value;
                OnPropertyChanged("ManImage");
            }
        }

        public string WordToGuess
        {
            get
            {
                return m_currentWord;
            }
            set
            {
                m_currentWord = value;
                OnPropertyChanged("WordToGuess");
            }
        }

        private char CharacterPressed
        {
            set
            {
                m_gameLogic.ModifyWord(value);

                WordToGuess = m_gameLogic.UserWord;
            }
        }

        private void ChangeMan(object parameter)
        {
            if (m_gameLogic.NumberOfWrongs > m_numberOfMistakes)
            {
                ++m_numberOfMistakes;
                ManImage = m_imageHandlerMan.NextImage;
            }
            else
                ManImage = m_imageHandlerMan.ActualImage;
        }

        private void Reset()
        {
            WordToGuess = m_gameLogic.UserWord;
            ManImage = m_imageHandlerMan.ResetImage;
            m_numberOfMistakes = 0;
            ResetTimer();
            OnPropertyChanged("WrongCharacterText");
            OnPropertyChanged("NumberOfLevels");
            OnPropertyChanged("Category");
        }

        private void ResetTimer()
        {
            if (TimerText != null)
            {
                m_numberOfSeconds = 31;
                m_deadline = DateTime.Now.AddSeconds(m_numberOfSeconds);
                m_timer.IsEnabled = false;
                TimerText = "Time:30";
            }
        }

        private void AboutMessage(object parameter)
        {
            MessageBox.Show("Aceasta aplicatie a fost realizata de catre studenta" +
                " Lixandru Andreea Bianca, din grupa 382, specializare informatica aplicata!"); ;
        }

        public ICommand About
        {
            get
            {
                if (m_aboutCommand == null)
                    m_aboutCommand = new RelayCommand(AboutMessage);
                return m_aboutCommand;
            }
        }

        private bool CheckGameStatus()
        {
            switch (m_gameLogic.IsEnd())
            {
                case LogicGame.GameStatus.WIN:
                    {
                        m_timer.Stop();
                        MessageBox.Show("You Win this game! Congratulations!");
                        Reset();
                        break;
                    }
                case LogicGame.GameStatus.WIN_LEVEL:
                    {
                        Reset();
                        break;
                    }
                case LogicGame.GameStatus.NOT_GUESSED:
                    {
                        ManImage = m_imageHandlerMan.NextImage;
                        m_timer.Stop();
                        MessageBox.Show("Sorry! Please try again!");
                        Reset();
                        break;
                    }
                case LogicGame.GameStatus.STILL_GUESSING:
                    {
                        return true;
                    }
            }
            return false;
        }

        private void CharacterIsPressed(object parameter)
        {
            CharacterPressed = parameter.ToString()[parameter.ToString().Length - 1];

            if (CheckGameStatus() == true)
                ChangeMan(parameter);

            OnPropertyChanged("WrongCharacterText");

            if (m_timer.IsEnabled == false)
            {
                m_deadline = DateTime.Now.AddSeconds(m_numberOfSeconds);
                m_timer.Start();
            }
        }

        public ICommand KeyPressed
        {
            get
            {
                if (m_characterPressed == null)
                    m_characterPressed = new RelayCommand(CharacterIsPressed);
                return m_characterPressed;
            }
        }

        private void ChangeCat(object parameter)
        {
            string text = parameter.ToString();

            string category = text.Substring(text.IndexOf(":") + 1);
            category = category.Substring(0, category.IndexOf(" "));

            switch (category)
            {
                case "Animals":
                    {
                        WordToGuess = m_gameLogic.ChangeToAnimalsCategory;
                        break;
                    }
                case "Cars":
                    {
                        WordToGuess = m_gameLogic.ChangeToCarsCategory;
                        break;
                    }
                case "Flowers":
                    {
                        WordToGuess = m_gameLogic.ChangeToFlowersCategory;
                        break;
                    }
                case "Insects":
                    {
                        WordToGuess = m_gameLogic.ChangeToInsectsCategory;
                        break;
                    }
                case "Movies":
                    {
                        WordToGuess = m_gameLogic.ChangeToMoviesCategory;
                        break;
                    }

                default:
                    {
                        WordToGuess = m_gameLogic.ChangeToAllCategories;
                        break;
                    }
            }
            Reset();
        }

        public ICommand ChangeCategory
        {
            get
            {
                if (m_changeCategory == null)
                    m_changeCategory = new RelayCommand(ChangeCat);
                return m_changeCategory;
            }
        }

        private void CloseWindow(object parameter)
        {
            Window currentWindow = (Window)parameter;

            currentWindow.Close();
        }

        public ICommand ExitWindow
        {
            get
            {
                if (m_exitGameWindow == null)
                    m_exitGameWindow = new RelayCommand(CloseWindow);
                return m_exitGameWindow;
            }
        }

        private void Save(object parameter)
        {
            int numberOfSeconds = -1;

            if (TimerText != null)
                numberOfSeconds = int.Parse(TimerText.Substring(TimerText.IndexOf(':') + 1));

            m_gameLogic.SaveGameHistoric(numberOfSeconds);
            NewGame(null);
        }

        public ICommand SaveGame
        {
            get
            {
                if (m_saveGame == null)
                    m_saveGame = new RelayCommand(Save);
                return m_saveGame;
            }
        }

        private void GenerateStats(object parameter)
        {
            string str = m_gameLogic.GetStats();

            MessageBox.Show(str);
        }

        public ICommand GenerateStatistics
        {
            get
            {
                if (m_statistics == null)
                    m_statistics = new RelayCommand(GenerateStats);
                return m_statistics;
            }
        }

        private void Open(object parameter)
        {
            m_gameLogic.GetGame();
            WordToGuess = m_gameLogic.UserWord;
            m_numberOfMistakes = m_gameLogic.NumberOfWrongs;
            ManImage = m_imageHandlerMan.ResetImage;
            TimerText = m_gameLogic.TimeRemained;

            if (TimerText != null)
            {
                m_numberOfSeconds = int.Parse(TimerText.Substring(TimerText.IndexOf(':') + 1));
                m_deadline = DateTime.Now.AddSeconds(m_numberOfSeconds);
                m_timer.Start();
            }

            for (int index = 0; index < m_numberOfMistakes; ++index)
                ManImage = m_imageHandlerMan.NextImage;

            OnPropertyChanged("WrongCharacterText");
            OnPropertyChanged("NumberOfLevels");
            OnPropertyChanged("Category");
        }

        public ICommand OpenGame
        {
            get
            {
                if (m_openGame == null)
                    m_openGame = new RelayCommand(Open);
                return m_openGame;
            }
        }

        private void NewGame(object parameter)
        {
            m_gameLogic.ResetGame();
            Reset();
        }

        public ICommand StartNewGame
        {
            get
            {
                if (m_newGame == null)
                    m_newGame = new RelayCommand(NewGame);
                return m_newGame;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
