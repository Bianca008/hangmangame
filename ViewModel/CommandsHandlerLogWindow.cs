﻿using Hangman.Model;
using Hangman.View;
using HangmanGame.Model;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;

namespace Hangman.ViewModel
{
    class CommandsHandlerLogWindow : INotifyPropertyChanged
    {
        ICommand m_leftArrowCommand;
        ICommand m_rightArrowCommand;
        ICommand m_gameWindow;

        public CommandsHandlerLogWindow()
        {
            m_imageHandler = new ImageHandler();
        }

        ImageHandler m_imageHandler;

        public string ImageSource
        {
            get
            {
                return m_imageHandler.ActualImage;
            }

            private set
            {
                m_imageHandler.ActualImage = value;
                OnPropertyChanged("ImageSource");
            }
        }

        private void LeftArrow(object parameter)
        {
            ImageSource = m_imageHandler.PreviousImage;
        }

        private void RightArrow(object parameter)
        {
            ImageSource = m_imageHandler.NextImage;
        }

        public ICommand ImageCommandLeftArrow
        {
            get
            {
                if (m_leftArrowCommand == null)
                    m_leftArrowCommand = new RelayCommand(LeftArrow);
                return m_leftArrowCommand;
            }
        }

        public ICommand ImageCommandRightArrow
        {
            get
            {
                if (m_rightArrowCommand == null)
                    m_rightArrowCommand = new RelayCommand(RightArrow);
                return m_rightArrowCommand;
            }
        }

        void OpenGameWindow(object parameter)
        {
            DataGrid dataGrid = (DataGrid)parameter;
            Player player = (Player)dataGrid.SelectedItem;

            if (player != null)
            {
                GameWindow gameWindow = new GameWindow(player.Name, player.Avatar);

                gameWindow.Show();
            }
        }

        public ICommand GameWindow
        {
            get
            {
                if (m_gameWindow == null)
                    m_gameWindow = new RelayCommand(OpenGameWindow);
                return m_gameWindow;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
